// get & set data
let _DATA =[]
let dataOBJ = {
  id:0,
  type:'',
  price :'',
  day:'',
  des: ''
}

$('#form_BTN').on('click',function(){
  if($('.moneyTypeinput:checked').val() == undefined ){
    $( ".moneyType" ).addClass( 'input_error')
    $('p#errorText').show()
  }else if($('#price_input').val() == ""){
    $( '#price_input' ).addClass( 'input_error')
    $('p#errorText').show()
  }else if($('#time_input_d').val() == ""){
    $( "#time_input_d" ).addClass( 'input_error')
    $('p#errorText').show()
  }else if($('#time_input_m').val() == ""){
    $( "#time_input_m" ).addClass( 'input_error')
    $('p#errorText').show()
  }else if($('#time_input_y').val() == ""){
    $( "#time_input_y" ).addClass( 'input_error')
    $('p#errorText').show()
  }else if($('#des_input').val() == ""){
    $( "#des_input" ).addClass( 'input_error')
    $('p#errorText').show()
  }else{
    dataOBJ.type = $('.moneyTypeinput:checked').attr('id')
    dataOBJ.price = parseInt($( '#price_input' ).val())
    let day_ = $( "#time_input_d" ).val()
    let month_ = $( "#time_input_m" ).val()
    let year_ = $( "#time_input_y" ).val()
    let _DAYTE = `${year_}/${month_}/${day_}`
    dataOBJ.day = _DAYTE
    dataOBJ.des = $( "#des_input" ).val()
    
    let objZX = JSON.parse(localStorage.getItem('DATA')||'[]') != null || JSON.parse(localStorage.getItem('DATA')||'[]') != undefined ? JSON.parse(localStorage.getItem('DATA')||'[]') : 0
    let getID = objZX != 0 ? objZX[0].id : 0
    let newID = getID + 1
    dataOBJ.id = newID

    _DATA.push(dataOBJ)
    _DATA = _DATA.concat(JSON.parse(localStorage.getItem('DATA')||'[]'));
    localStorage.setItem('DATA', JSON.stringify(_DATA));

    let objX = JSON.parse(localStorage.getItem('DATA')||'[]')
    let _in = []
    let _out = []
    let _inSum = 0
    let _outSum = 0
    let sumPrice = [{in:'',out:''},{inArr:[],outArr:[]}]
    
    for (let i in objX) {
      if(objX[i].type == 'income') {
          _in.push(objX[i].price)
          _inSum = _inSum+objX[i].price
      }else{
          _out.push(objX[i].price)
          _outSum = _outSum+objX[i].price
      }
    }
    sumPrice[0].in = _inSum
    sumPrice[0].out = _outSum
    sumPrice[1].inArr = _in
    sumPrice[1].outArr = _out
    localStorage.setItem('SUM__', JSON.stringify(sumPrice));

    window.location.reload()
  }
})
sumHandler()

function sumHandler(){
  
  _SUMGET = JSON.parse(localStorage.getItem('SUM__'));
  if(_SUMGET == undefined || '' || null){
    $('#income_p_view').html('000.0')
    $('#outcome_p_view').html('000.0')
  }else{
    _SUMGET[0].in = _SUMGET[0].in.toString()
    _SUMGET[0].out = _SUMGET[0].out.toString()
    
    $('#income_p_view').html(tocur(_SUMGET[0].in))
    $('#outcome_p_view').html(tocur(_SUMGET[0].out))

    $('#income_p_view').html(tocur(_SUMGET[0].in))
    $('#outcome_p_view').html(tocur(_SUMGET[0].out))
  }

}

function tocur (amount) {
  return amount
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")+ " تومان ";
}

$('input').on('focus',function(){
  $(this).removeClass('input_error')
  if($('.input_error').length == 0){
    $('p#errorText').hide()
  }
})
$('textarea').on('focus',function(){
  $(this).removeClass('input_error')
  if($('.input_error').length == 0){
    $('p#errorText').hide()
  }
})
$('.moneyTypeinput').click(function() {
  if ($(this).is(':checked')) {
    $( ".moneyType" ).removeClass( 'input_error')
    $('p#errorText').hide()
  }
});

$('#price_input').on('input propertychange', function(){
  let price_input_val = $(this).val()
  let price_numToWord = Num2persian(price_input_val)
  $('#price_view').html(price_numToWord)
});

let _innerHTML = ''
let _innerHtmlDATA = JSON.parse(localStorage.getItem('DATA')||'[]')
let b =[]
let inP = 'درآمد'
let outP = 'هزینه'
let green = '#2cbe26'
let red = '#e03f3f'
for (j in _innerHtmlDATA){
  b.unshift(_innerHtmlDATA[j])
}
let dataID = 0
for (o in b){
  dataID = b[o].id
  _innerHTML += '<tr>'
  _innerHTML += '<td data-id="'+dataID+'" class="table_id">'+b[o].id+'</td>'
  _innerHTML += '<td class="table_price">'+tocur(b[o].price)+ '</td>'
  _innerHTML += '<td class="table_date">'+b[o].day+'</td>'
  _innerHTML += '<td class="table_money_type" style="color:'+(b[o].type == "income" ? green : red)+' ">'+(b[o].type == "income" ? inP : outP)+'</td>'
  _innerHTML += '<td data-label="توضیحات">'
  _innerHTML += '<div data-id="'+dataID+'" data-label="view" class="modal_btn">نمایش</div>'
  _innerHTML += '<div data-id="'+dataID+'" data-label="remove" class="modal_btn">حذف</div>'
  _innerHTML += '</td></tr>'
}
$('tbody').html(_innerHTML)


//chart
const labels = [
    '',
    'فروردین ۰۱',
    'اردیبهشت ۰۱',
    'خرداد ۰۱',
  ];

  let fakeCharData = {inArr:[0,1,2,3,4,5],outArr:[0,1,2,3,4,5]}
  let getChartData = JSON.parse(localStorage.getItem('SUM__'))
  let _chartData = getChartData != null ? getChartData[1] : fakeCharData
  const dataOutcome = {
    labels: labels,
    datasets: [{
      label: 'هزینه',
      backgroundColor: '#e03f3f',
      borderColor: '#e03f3f',
      data: _chartData.outArr.reverse(),
    },{
      label: 'درآمد',
      backgroundColor: '#2cbe26',
      borderColor: '#2cbe26',
      data: _chartData.inArr.reverse(),
    }]
  };

const config = {
  type: 'line',
  data: dataOutcome,
  options: {
    responsive: true,
  }
};

// modal
var modal_des = $("#modal_view_des");
var modal_rem = $("#modal_view_remove");
var modal = $(".modal");
var open_btn = $(".modal_btn");
var remove_btn = $("#modal_des_remove_btn");
var close = $(".close_modal");
var modalBody = $('.modal-content')
let IDtdModal
let IDbtnModal

open_btn.on('click',function() {
  IDtdModal = $(this).parent().parent().find('td[data-id]').attr('data-id')
  IDbtnModal = $(this).attr('data-id')
  if($(this).attr('data-label') == 'view'){
    for(t in _innerHtmlDATA){
      if(_innerHtmlDATA[t].id == IDtdModal && _innerHtmlDATA[t].id == IDbtnModal){
        $('#modal_des').html(_innerHtmlDATA[t].des)
      }
    }
    modal_des.show()
    IDtdModal = $(this).parent().parent().find('td[data-id]').attr('data-id')
  }else{
    modal_rem.show()
  }
})

close.click (function() {
  modal.hide();
})

remove_btn.on('click',function(){
  for(e in _innerHtmlDATA){
    if(_innerHtmlDATA[e].id == IDtdModal){
      _innerHtmlDATA.splice(e,1)
      localStorage.setItem('DATA', JSON.stringify(_innerHtmlDATA));
      window.location.reload()
    }
  }
})